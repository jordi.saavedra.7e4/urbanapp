package com.example.urbanapp

import com.google.firebase.firestore.GeoPoint

data class Marker(
    var type: String?=null,
    var coordinates: GeoPoint?=null,
    val email: String?=null,
    var image: String?=null
)