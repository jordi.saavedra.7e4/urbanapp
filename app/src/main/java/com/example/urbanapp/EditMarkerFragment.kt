package com.example.urbanapp

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.urbanapp.databinding.EditMarkerFragmentBinding
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class EditMarkerFragment:Fragment() {
    private val  db = FirebaseFirestore.getInstance()
    private var _binding: EditMarkerFragmentBinding? = null
    private val vm:MapViewModel by activityViewModels()
    private val args:EditMarkerFragmentArgs by navArgs()
    private var image : String? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = EditMarkerFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val arrayAdapter = ArrayAdapter.createFromResource(
            this.requireContext(),
            R.array.marker_type_array,
            android.R.layout.simple_spinner_item)

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.type.adapter = arrayAdapter

        val id = args.markerId
        var marker = Marker()

        db.collection("Markers").document(id).get().addOnSuccessListener {
            marker = it.toObject(Marker::class.java)!!

            binding.latitude.text = marker.coordinates?.latitude.toString()
            binding.longitude.text = marker.coordinates?.longitude.toString()
            binding.type.setSelection(vm.getTypePosition(marker.type!!))
            binding.name.setText(it.id)

            image = marker.image
            Log.d("", ""+image)
            if (image != null && image != "") {
                loadImage(image!!)
            }
        }

        binding.editMarkerButton.setOnClickListener {
            val newMarker = binding.name.text.toString()
            if (id != newMarker)
                db.collection("Markers").document(id).delete()

            marker.type = binding.type.selectedItem.toString()
            marker.coordinates = GeoPoint(binding.latitude.text.toString().toDouble(), binding.longitude.text.toString().toDouble())

            db.collection("Markers").document(newMarker).set(marker)

            view.findNavController().navigate(R.id.action_editMarkerFragment_to_mapFragment)
        }

        binding.deleteMarkerButton.setOnClickListener {
            val markerName = binding.name.text.toString()
            db.collection("Markers").document(markerName).delete()

            FirebaseStorage.getInstance().reference.child("images/$image").delete()
            view.findNavController().navigate(R.id.action_editMarkerFragment_to_mapFragment)
        }
    }

    private fun loadImage(image:String){
        val storage = FirebaseStorage.getInstance().reference.child("images/$image")
        val localFile = File.createTempFile("temp", "jpeg")
        storage.getFile(localFile).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
            binding.image.setImageBitmap(bitmap)

        }.addOnFailureListener{
            Toast.makeText(requireContext(), "Error downloading image!", Toast.LENGTH_SHORT)
                .show()
        }
    }
}