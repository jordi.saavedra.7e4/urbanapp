package com.example.urbanapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.urbanapp.databinding.RegisterFragmentBinding
import com.google.firebase.auth.FirebaseAuth

class RegisterFragment:Fragment() {
    private var _binding: RegisterFragmentBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = RegisterFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.register.setOnClickListener {
            val email = binding.email.text.toString()
            val password = binding.password.text.toString()

            FirebaseAuth.getInstance().
            createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        view.findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
                        //val emailLogged = it.result?.user?.email
                        //goToHome(emailLogged!!)
                    }
                    else{
                        showError("Error al registrar l'usuari")
                    }
                }
        }
    }

    private fun showError(message:String){
        val toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
        toast.show()
    }
}