package com.example.urbanapp

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.urbanapp.databinding.MapFragmentBinding
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint


const val REQUEST_CODE_LOCATION = 100

class MapFragment: Fragment(), OnMapReadyCallback {
    private lateinit var map: GoogleMap
    private val  db = FirebaseFirestore.getInstance()
    private val vm:MapViewModel by activityViewModels()

    private var _binding: MapFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = MapFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createMap()

        binding.listButton.setOnClickListener {
            if (vm.loggedIn())
                view.findNavController().navigate(R.id.action_mapFragment_to_myMarkerListFragment)
            else
                Toast.makeText(requireContext(), "Has d'iniciar sessió per a poder anar a la teva llista de marcadors", Toast.LENGTH_SHORT).show()
        }

        binding.addMarkerButton.setOnClickListener {
            if (vm.loggedIn()){
                val latitude: Double
                val longitude: Double

                val coordinates = map.myLocation
                if (coordinates == null){
                    latitude = map.cameraPosition.target.latitude
                    longitude = map.cameraPosition.target.longitude
                }

                else{
                    latitude = coordinates.latitude
                    longitude = coordinates.longitude
                }

                val action = MapFragmentDirections.actionMapFragmentToAddMarkerFragment(latitude.toFloat(), longitude.toFloat())
                view.findNavController().navigate(action)
            }

            else
                Toast.makeText(requireContext(), "Has d'iniciar sessió per a poder afegir un marcador", Toast.LENGTH_SHORT).show()
        }

        binding.login.setOnClickListener {
            view.findNavController().navigate(R.id.action_mapFragment_to_loginFragment)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        setMarkersOnMap()
        enableLocation()

        map.setOnInfoWindowClickListener {
            val action = if (it.isDraggable) {
                MapFragmentDirections.actionMapFragmentToEditMarkerFragment(it.title.toString())
            } else {
                MapFragmentDirections.actionMapFragmentToViewMarkerFragment(it.title.toString())
            }

            requireView().findNavController().navigate(action)
        }

        map.setOnMarkerDragListener(object : OnMarkerDragListener {
            lateinit var marker:com.example.urbanapp.Marker
            override fun onMarkerDrag(marker: Marker) {

            }
            override fun onMarkerDragStart(m: Marker) {
                db.collection("Markers").document(m.title.toString())
                    .get()
                    .addOnSuccessListener {
                        marker = it.toObject(com.example.urbanapp.Marker::class.java)!!
                    }
            }

            override fun onMarkerDragEnd(m: Marker) {
                marker.coordinates = GeoPoint(m.position.latitude, m.position.longitude)

                db.collection("Markers").document(m.title.toString()).set(
                    marker
                )
            }
        })
    }

    private fun createMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun createMarker(name:String, geoPoint: GeoPoint, email:String){
        val coordinates = LatLng(geoPoint.latitude, geoPoint.longitude)
        val newMarker = MarkerOptions().position(coordinates).title(name)
        newMarker.draggable(vm.isItMyMarker(email))

        map.addMarker(newMarker)
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }

        setMarkersOnMap()
    }

    private fun setMarkersOnMap(){
        db.collection("Markers")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val name = document.id
                    val coordinates = document.get("coordinates") as GeoPoint
                    val email = document.get("email") as String
                    createMarker(name, coordinates, email)
                }
            }
            .addOnFailureListener { exception ->
                Log.d("e", "Error getting documents: ", exception)
            }
    }
}