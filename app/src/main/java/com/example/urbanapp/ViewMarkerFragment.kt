package com.example.urbanapp

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.urbanapp.databinding.ViewMarkerFragmentBinding
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class ViewMarkerFragment: Fragment() {
    private val  db = FirebaseFirestore.getInstance()
    private var _binding: ViewMarkerFragmentBinding? = null
    private val args: ViewMarkerFragmentArgs by navArgs()
    private var image : String? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = ViewMarkerFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = args.markerId

        db.collection("Markers").document(id).get().addOnSuccessListener {
            val marker = it.toObject(Marker::class.java)!!

            binding.latitude.text = marker.coordinates?.latitude.toString()
            binding.longitude.text = marker.coordinates?.longitude.toString()
            binding.type.text = marker.type
            binding.name.text = it.id

            image = marker.image
            Log.d("", ""+image)
            if (image != null && image != "") {
                loadImage(image!!)
            }
        }
    }

    private fun loadImage(image:String){
        val storage = FirebaseStorage.getInstance().reference.child("images/$image")
        val localFile = File.createTempFile("temp", "jpeg")
        storage.getFile(localFile).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
            binding.image.setImageBitmap(bitmap)

        }.addOnFailureListener{
            Toast.makeText(requireContext(), "Error downloading image!", Toast.LENGTH_SHORT)
                .show()
        }
    }
}