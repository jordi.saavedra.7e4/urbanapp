package com.example.urbanapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.urbanapp.databinding.MyMarkerListFragmentBinding
import com.google.firebase.firestore.*

class MyMarkerListFragment:Fragment() {
    private var _binding: MyMarkerListFragmentBinding? = null
    private val binding get() = _binding!!
    private val  db = FirebaseFirestore.getInstance()
    private val vm:MapViewModel by activityViewModels()
    lateinit var mAdapter: MarkerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = MyMarkerListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.title.text = vm.userEmail

        setupRecyclerView()
        eventChangeListener()

        binding.addMarkerButton.setOnClickListener {
            val action = MyMarkerListFragmentDirections.actionMyMarkerListFragmentToAddMarkerFragment(0f, 0f)
            view.findNavController().navigate(action)
        }
    }

    private fun setupRecyclerView() {
        mAdapter = MarkerAdapter()
        binding.markerList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
    }


    private fun eventChangeListener() {
        Log.d("d", vm.userEmail)
        db.collection("Markers").whereEqualTo("email", vm.userEmail).addSnapshotListener(object: EventListener<QuerySnapshot> {
            override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                if(error != null){
                    Log.e("Firestore error", error.message.toString())
                    return
                }
                val m = mutableListOf<MarkerItem>()
                for(dc: DocumentChange in value?.documentChanges!!){
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                            val type = dc.document.get("type")
                            val coordinates = dc.document.get("coordinates")
                            val name = dc.document.id
                            m.add(MarkerItem(name,type as String?, coordinates as GeoPoint?))
                        }
                        DocumentChange.Type.REMOVED -> {
                            val removedMarker = dc.document.toObject(Marker::class.java)
                            vm.markers.remove(removedMarker)
                        }
                        DocumentChange.Type.MODIFIED -> {
                            val editedMarker = dc.document.toObject(Marker::class.java)
                            //mapViewModel.markers.get
                        }
                    }
                }
                mAdapter.setMarkers(m)
                Log.d("d", ""+m.size)
            }
        })
    }

}