package com.example.urbanapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.auth.User

class MarkerAdapter : RecyclerView.Adapter<MarkerAdapter.MarkerViewHolder>() {
    private var markers = mutableListOf<MarkerItem>()

    fun setMarkers(markerList:MutableList<MarkerItem>){
        markers = markerList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarkerViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.marker_item, parent, false)
        return MarkerViewHolder(view)
    }

    override fun onBindViewHolder(holder: MarkerViewHolder, position: Int) {
        holder.bindData(markers[position])
    }

    override fun getItemCount(): Int {
        return markers.size
    }

    inner class MarkerViewHolder(itemView:View): RecyclerView.ViewHolder(itemView){
        private var name:TextView = itemView.findViewById(R.id.marker_name)
        fun bindData(marker: MarkerItem){
            name.text = marker.name

            itemView.setOnClickListener {
                val action = MyMarkerListFragmentDirections.actionMyMarkerListFragmentToEditMarkerFragment(marker.name!!)
                itemView.findNavController().navigate(action)
            }
        }
    }
}