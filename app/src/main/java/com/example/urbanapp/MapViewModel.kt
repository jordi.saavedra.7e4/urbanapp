package com.example.urbanapp

import android.util.Log
import androidx.lifecycle.ViewModel

class MapViewModel : ViewModel() {
    var markers = mutableListOf<Marker>()
    var userEmail = "jordi@gmail.com"

    fun getTypePosition(type: String):Int{
        var pos = 0
        when(type){
            "graffity" -> pos = 0
            "mural" -> pos = 1
            "sculpture" -> pos = 2
            "poster" -> pos = 3
            "other" -> pos = 4
        }
        return pos
    }

    fun isItMyMarker(markerEmail:String):Boolean{
        return userEmail == markerEmail
    }

    fun loggedIn():Boolean{
        return userEmail != ""
    }

}