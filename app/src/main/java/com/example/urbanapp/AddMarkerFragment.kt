package com.example.urbanapp

import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStoragePublicDirectory
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.urbanapp.databinding.AddMarkerFragmentBinding
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.storage.FirebaseStorage
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AddMarkerFragment:Fragment() {
    private var _binding: AddMarkerFragmentBinding? = null
    private val vm:MapViewModel by activityViewModels()
    private val  db = FirebaseFirestore.getInstance()
    private val args:AddMarkerFragmentArgs by navArgs()
    private val binding get() = _binding!!

    private lateinit var imageUri:Uri
    private lateinit var resultLauncher: ActivityResultLauncher<Intent>
    private lateinit var imagePath:String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = AddMarkerFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val arrayAdapter = ArrayAdapter.createFromResource(
            this.requireContext(),
            R.array.marker_type_array,
            android.R.layout.simple_spinner_item)

        resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                result ->
            if (result.resultCode == RESULT_OK) {
                val data: Intent? = result.data
                if (data != null) {
                    imageUri = data.data!!
                    binding.image.setImageURI(imageUri)
                }
            }
        }

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.markerType.adapter = arrayAdapter
        binding.markerType.setSelection(0)
        binding.markerLatitude.text = args.latitude.toString()
        binding.markerLongitude.text = args.longitude.toString()

        binding.addMarkerButton.setOnClickListener {
            val name = binding.markerName.text.toString()
            val imageName = getFileName()

            val marker = Marker(
                binding.markerType.selectedItem.toString(),
                GeoPoint(
                    binding.markerLatitude.text.toString().toDouble(),
                    binding.markerLongitude.text.toString().toDouble()),
                vm.userEmail,
                imageName
            )

            db.collection("Markers").document(name).set(marker)

            uploadImage(imageName)

            view.findNavController().navigate(R.id.action_addMarkerFragment_to_mapFragment)
        }

        binding.takePhoto.setOnClickListener {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                    // Create the File where the photo should go
                    val photoFile: File? = try {
                        createImageFile()
                    } catch (ex: IOException) {
                        // Error occurred while creating the File
                        null
                    }
                    // Continue only if the File was successfully created
                    photoFile?.also {
                        imageUri = FileProvider.getUriForFile(
                            this.requireContext(),
                            "com.example.android.fileprovider",
                            it
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                        startActivityForResult(takePictureIntent, 1)
                    }
            }

        }

        binding.selectPhoto.setOnClickListener {
            selectImage()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            binding.image.setImageURI(imageUri)
        }
    }


    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = getFileName()
        val storageDir: File = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            timeStamp,
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            imagePath = name
        }
    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncher.launch(intent)
    }

    private fun uploadImage(fileName:String){
        val storage = FirebaseStorage.getInstance().getReference("images/$fileName")
        storage.putFile(imageUri)
            .addOnSuccessListener {
                Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener {
                Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
            }
    }

    private fun getFileName(): String {
        val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
        val now = Date()

        return formatter.format(now)
    }

}