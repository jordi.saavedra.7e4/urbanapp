package com.example.urbanapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.urbanapp.databinding.LoginFragmentBinding
import com.google.firebase.auth.FirebaseAuth

class LoginFragment:Fragment(){
    private var _binding: LoginFragmentBinding? = null

    private val binding get() = _binding!!
    private val vm:MapViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = LoginFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.login.setOnClickListener {
            val password = binding.password.editableText.toString()
            val email = binding.email.editableText.toString()

            FirebaseAuth.getInstance().
            signInWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        vm.userEmail = it.result.user.email
                        view.findNavController().navigate(R.id.action_loginFragment_to_mapFragment)
                    }
                    else{
                        showError("Error al fer login")
                    }
                }
        }

        binding.register.setOnClickListener {
            view.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }

    }

    private fun showError(message:String){
        val toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
        toast.show()
    }
}