package com.example.urbanapp

import com.google.firebase.firestore.GeoPoint

data class MarkerItem(
    var name: String?=null,
    var type: String?=null,
    var coordinates: GeoPoint?=null,
)